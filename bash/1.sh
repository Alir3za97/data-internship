# 1.1
for filename in 1.txt 2.py 3.pyc 4.pyc raw.txt
do
	touch $filename
done

mkdir dir1
touch dir1/test.pyc
touch dir1/test2.pyc

# 1.2
find . -name '*.pyc' | xargs rm -rf

# 1.3
mkdir desk
ls -p | grep -v / | xargs -I{} cp {} desk

# 1.4
sudo apt install sl
#sl

# 1.5
tar -xf bash-playground.tar.gz

# 1.6
find bash-playground/ | grep json | xargs cat >> append.json

# 1.7
cat append.json | grep -v great > filtered.json

# 1.8
wc -l filtered.json

# 1.9
ls -l --block-size=K | grep filtered.json | awk '{print $5}'

# 1.10
# less filtered.json and search with /

# 1.11
tail -n 1000 filtered.json >> thousand.json

# 1.12
df -h

# 1.13
sudo apt install htop
# htop and F1 to see meaning of colours
# used/buffer/cache
