import sys
import json

def copy(source, destination):
    with open(source, 'r') as f:
        lines = f.read().split('\n')
    with open(destination, 'w') as f:
        f.write('\n'.join(
            json.dumps(out_line)
            for out_line in sorted(
                [json.loads(line) for line in lines if line],
                key=lambda x: x['id']
            )
        ))

copy(sys.argv[1], sys.argv[2])
