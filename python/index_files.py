import os
import sys

def walk(directory):
    os.chdir(directory)
    for child in os.listdir():
        child_path = os.path.join(directory, child)
        if os.path.isdir(child_path):
            for subdir_child in walk(child_path):
                yield subdir_child
        else:
            yield child_path
    os.chdir(os.pardir)

directory = os.path.abspath(sys.argv[1])
with open(os.path.join(directory, 'index'), 'w') as f:
    f.write('\n'.join(walk(directory)))

