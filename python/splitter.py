def split(number):
    if not isinstance(number, str):
        number = str(number)
    return ','.join([
            number[-idx:] if idx == 3 else number[-idx:-idx+3]
             for idx in reversed(range(3, len(number) + 3, 3))
    ])

