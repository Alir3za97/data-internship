
def normalize(string):
    PERSIAN_NUMERIC_CHARS = '۰۱۲۳۴۵۶۷۸۹'
    LATIN_NUMERIC_CHARS = '0123456789'
    def normalize_char(char):
        idx = PERSIAN_NUMERIC_CHARS.find(char)
        return char if idx == -1 else LATIN_NUMERIC_CHARS[idx]

    return ''.join(normalize_char(char) for char in string)

print(normalize('12۳۴56۷۸9۰'))
