import sys
import re

from collections import defaultdict

class TaskManager:
    HELP_MESSAGE = """
    create user: CREATE USER {user}
    create task: CREATE TASK {task}
    list task users: LIST TASK {task}
    list user tasks: LIST USER {user}
    assign: ASSIGN {task} {user}
    """
    QUERY_PATTERNS = (
        (re.compile("LIST USER\s+(?P<user>\w*)\s*"), 'list_user_tasks'),
        (re.compile("LIST TASK\s+(?P<task>\w*)\s*"), 'list_task_users'),
        (re.compile("CREATE\s+USER\s+(?P<user>\w*)\s*"), 'add_user'),
        (re.compile("CREATE\s+TASK\s+(?P<task>\w*)\s*"), 'add_task'),
        (re.compile("ASSIGN\s+(?P<task>\w+)\s+(?P<user>\w+)\s*"), 'assign')
    )

    def __init__(self):
        self.tasks = set()
        self.users = set()
        self.user_tasks = defaultdict(set)
        self.task_users = defaultdict(set)

    def assign(self, task, user):
        if user not in self.users:
            raise Exception("User {} does not exist".format(user))
        if task not in self.tasks:
            raise Exception("Task {} does not exist".format(task))
        self.user_tasks[user].add(task)
        self.task_users[task].add(user)

    def add_task(self, task):
        self.tasks.add(task)

    def add_user(self, user):
        self.users.add(user)

    def list_user_tasks(self, user):
        print(', '.join(self.user_tasks[user]))

    def list_task_users(self, task):
        print(', '.join(self.task_users[task]))

    def input(self):
        inp = input()
        for pattern, func in TaskManager.QUERY_PATTERNS:
            match = pattern.match(inp)
            if match:
                getattr(self, func)(**match.groupdict())
                return
        raise Exception('Unknown command')

    def run(self, n_query=-1):
        while n_query:
            self.input()
            n_query -= 1


args = sys.argv[1:]
if '--help' in args:
    print(TaskManager.HELP_MESSAGE)
elif '-n' in args:
    idx = args.index('-n')
    if idx + 1 >= len(args):
        print('Pass number of queries')
    else:
        n_query = int(args[idx + 1])
        TaskManager().run(n_query)
else:
    TaskManager().run()
